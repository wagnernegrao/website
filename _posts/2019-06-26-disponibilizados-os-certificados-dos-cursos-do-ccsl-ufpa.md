---
layout: post
author: Filipe Saraiva
category: newspaper-o
title: Disponibilizados os Certificados dos Cursos do CCSL-UFPA
---

Demorou mas chegou! Finalmente os certificados dos cursos livres realizados pelo __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) estão disponíveis!

No momento temos os certificados para os cursos "Linux Fundamental" e "Python Fundamental". Todos os alunos que compareceram a pelo menos 75% das aulas tiveram os certificados gerados.

Para baixá-lo, visite o [repositório dos certificados](https://gitlab.com/ccsl-ufpa/cursos-livres-certificados) no Gitlab e navegue pela estrutura de diretórios a partir do nome do curso que você fez. Em seguida, entre na pasta correspondente ao ano do curso (no momento há apenas 2019) e depois, na pasta "certificados".

Por exemplo, quem fez o curso "Linux Fundamental" em 2019 deverá entrar na pasta `Linux Fundamental/2019/certificados`.

Os certificados estarão nessa pasta, em arquivos que levam o nome dos alunos. Clicando em algum deles, há uma opção para download disponibilizada em um ícone de nuvem com seta para baixo, na barra onde se encontra o nome do arquivo.

Mas para quem quiser ir direto aos certificados, clique nos links abaixo:

* [Linux Fundamental - 2019](https://gitlab.com/ccsl-ufpa/cursos-livres-certificados/blob/master/Linux Fundamental/2019/certificados/)
* [Python Fundamental - 2019](https://gitlab.com/ccsl-ufpa/cursos-livres-certificados/tree/master/Python%20Fundamental/2019/certificados)

Com a estrutura de emissão dos certificados montada, a geração dos certificados dos próximos cursos será mais rápida.

E fiquem atentos para a divulgação dos próximos cursos do CCSL-UFPA!
